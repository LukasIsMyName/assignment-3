//Function that validates the form, checks if the name is valid and such
function validateForm()
{
    var attempt = true;
    var x = document.forms["greetingsForm"]["userName"].value;
    if(x == "")
    {
        alert("Please enter your name before diving in");
        return false;
    }
    else
    {
        alert("Welcome " + document.forms["greetingsForm"]["userName"].value);
        addname();
    }
}

//Function that adds the name to the query and changes inner html text
function addname()
{
    var x = document.getElementById("welcomeHeader");
    var queryString = document.location.search;
    var nameAdd = queryString.split("=")[1];
    if ( nameAdd == undefined)
    {
        console.log("Name not found in the query string");
    }
    else
    {
        x.innerText = "Hello, " + nameAdd;
    }

}

addname();